package facci.easv.examen2p.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import facci.easv.examen2p.Adapter.BadAdapter;
import facci.easv.examen2p.Adapter.BreakingAdapter;
import facci.easv.examen2p.Model.BreakingBad;
import facci.easv.examen2p.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    BadAdapter badAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.list_person);
        listBreaking();
    }

    public void listBreaking(){
        BreakingAdapter breakingAdapter = new BreakingAdapter();
        Call<List<BreakingBad>> call = breakingAdapter.getEpisode();
        call.enqueue(new Callback<List<BreakingBad>>() {
            @Override
            public void onResponse(Call<List<BreakingBad>> call,  Response<List<BreakingBad>> response) {
                List<BreakingBad> list = response.body();
                for(BreakingBad breakingBad : list){
                    breakingBad.getTitle();
                }
                badAdapter = new BadAdapter(MainActivity.this, list);
                listView.setAdapter(badAdapter);
            }

            @Override
            public void onFailure(Call<List<BreakingBad>> call, Throwable t) {
                Log.e("ERROR", "Error xd");
                Toast.makeText(MainActivity.this, "Fail app", Toast.LENGTH_SHORT).show();
            }


        });
    }

}