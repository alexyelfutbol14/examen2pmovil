package facci.easv.examen2p.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import facci.easv.examen2p.R;

public class SecondActivity extends AppCompatActivity {

    TextView idView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        idView = findViewById(R.id.idView);

        String id = getIntent().getStringExtra("_id");

        idView.setText("Hi" + id);
    }

}