package facci.easv.examen2p.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;


import java.util.List;

import facci.easv.examen2p.Model.BreakingBad;
import facci.easv.examen2p.R;
import facci.easv.examen2p.UI.SecondActivity;

public class BadAdapter extends BaseAdapter {
    private Context context;
    private List<BreakingBad> list;

    public BadAdapter(Context context, List<BreakingBad> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {return list.size();}

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if(convertView == null){
            convertView = View.inflate(context,  R.layout.list_bad, null);
        }

        TextView id = convertView.findViewById(R.id.txtId);
        TextView url = convertView.findViewById(R.id.txtUrl);
        //ImageView img = convertView.findViewById(R.id.image);

        BreakingBad breakingBad = list.get(position);

        //Glide.with(context).load(modelTest.getImg()).into(img);
        id.setText(breakingBad.getEpisode_id());
        url.setText(breakingBad.getTitle());


        return convertView;
    }

}
