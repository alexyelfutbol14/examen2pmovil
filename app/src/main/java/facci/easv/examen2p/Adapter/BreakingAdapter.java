package facci.easv.examen2p.Adapter;

import java.util.List;

import facci.easv.examen2p.Constans.BreakingAPI;
import facci.easv.examen2p.Model.BreakingBad;
import facci.easv.examen2p.Services.BreakingService;
import retrofit2.Call;

public class BreakingAdapter extends BaseAdapter implements BreakingService{
    private BreakingService breaking;

    public BreakingAdapter(){
        super(BreakingAPI.BASE_URL);
        breaking = createService(BreakingService.class);
    }

    @Override
    public Call<List<BreakingBad>> getEpisode() {
        return breaking.getEpisode();
    }

}
