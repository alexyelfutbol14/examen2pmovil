package facci.easv.examen2p.Services;

import java.util.List;

import facci.easv.examen2p.Constans.BreakingAPI;
import facci.easv.examen2p.Model.BreakingBad;
import facci.easv.examen2p.Model.BreakingResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface BreakingService {
    @GET(BreakingAPI.END_POINT)
    Call<List<BreakingBad>> getEpisode();
}
