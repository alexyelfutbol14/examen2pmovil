package facci.easv.examen2p.Model;

import java.util.List;

public class BreakingResponse {

    private boolean status;
    private List<BreakingBad> breakingBadList;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<BreakingBad> getBreakingBadList() {
        return breakingBadList;
    }

    public void setBreakingBadList(List<BreakingBad> breakingBadList) {
        this.breakingBadList = breakingBadList;
    }
}
