package facci.easv.examen2p.Model;

import com.google.gson.annotations.SerializedName;

public class BreakingBad {
    @SerializedName("episode_id")
    private String episode_id;
    @SerializedName("title")
    private String title;
    @SerializedName("air_date")
    private String air_date;
    @SerializedName("episode")
    private String episode;
    @SerializedName("series")
    private String series;

    public String getEpisode_id() {
        return episode_id;
    }

    public void setEpisode_id(String episode_id) {
        this.episode_id = episode_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAir_date() {
        return air_date;
    }

    public void setAir_date(String air_date) {
        this.air_date = air_date;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }
}
